// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCL1GqitkJns5wI77QdhBaW3O3pSS212rw",
    authDomain: "ionfire-84263.firebaseapp.com",
    projectId: "ionfire-84263",
    storageBucket: "ionfire-84263.appspot.com",
    messagingSenderId: "370528343211",
    appId: "1:370528343211:web:e8f39233c233de058053ee",
    measurementId: "G-NQJMJ2FXSS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
